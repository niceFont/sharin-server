const fastify = require('fastify')({ logger: true });

fastify.get('/', async (req, res) => ({ hello: 'world' }));

const start = async () => {
  try {
    await fastify.listen(process.env.PORT);
    fastify.log.info(`server listening on Port ${process.env.PORT}`);
  } catch (error) {
    fastify.log.error(error);
    process.exit(1);
  }
};

start();
