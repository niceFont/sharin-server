FROM node:12.18.2-alpine3.12
RUN apk update
RUN apk add openssh-server 
RUN mkdir -p /var/run/sshd

EXPOSE 22
EXPOSE 8080
COPY . /app
WORKDIR /app
RUN npm install
CMD [ "node", "server.js" ]